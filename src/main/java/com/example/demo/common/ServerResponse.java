package com.example.demo.common;

import java.io.Serializable;

public class ServerResponse<T> implements Serializable {

    //最典型的就是各种容器类，如：List、Set、Map。
    private int status;
    private String msg;
    private T data;


    public ServerResponse(int status,String msg,T data){
        this.status=status;
        this.msg=msg;
        this.data=data;
    }

    public static<T> ServerResponse<T> failWithCodeMsg(int code, String msg) {
       return new ServerResponse<>(ResponseCode.NEED_LOGIN.getCode(),msg,null);
    }

    public int getStatus() {
        return status;
    }

    public String getMsg() {
        return msg;
    }

    public T getData() {
        return data;
    }

    public boolean isSucc(){
        return this.status ==ResponseCode.SUCCESS.getCode();
    }
    public static<T> ServerResponse<T> succWithMsgData(String msg,T data){
        return new ServerResponse<>(ResponseCode.SUCCESS.getCode(),msg,data);
    }

    public static<T> ServerResponse<T> failWithMsg(String msg){
        return new ServerResponse<>(ResponseCode.ERROR.getCode(),msg,null);
    }
    public static<T> ServerResponse<T> succRegiser(String msg){
        return new ServerResponse<>(ResponseCode.SUCCESS.getCode(),msg,null);
    }
    public static<T> ServerResponse<T> errorRegiser(String msg){
        return new ServerResponse<>(ResponseCode.ERROR.getCode(),msg,null);
    }
    public static<T> ServerResponse<T> succRealse(String msg){
        return new ServerResponse<>(ResponseCode.SUCCESS.getCode(),msg,null);
    }

}
