package com.example.demo.controller;

import com.example.demo.common.Cnst;
import com.example.demo.common.ResponseCode;
import com.example.demo.common.ServerResponse;
import com.example.demo.entity.Release;
import com.example.demo.entity.User;
import com.example.demo.service.IRouteService;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;

@RestController
@RequestMapping("/route")
public class RouteController {
    @Autowired
    private IRouteService iRouteService;

    @GetMapping("/pageQuery")
    private ServerResponse<PageInfo> pageQuery(@RequestParam(value = "currentPage",defaultValue = "1")int currentPage,
                                               @RequestParam(value = "pageSizeStr",defaultValue = "5")int pageSize,
                                               String cid, String title, HttpSession session ){
        int icid=0;

        User loginUser = null;
        int uid=0;
        if(session.getAttribute(Cnst.CURRENT_USER)==null){
            uid=0;
        }else {
            loginUser=(User) session.getAttribute(Cnst.CURRENT_USER);
            uid=loginUser.getUid();
        }

        if(cid!=null && cid.length()>0 && !"null".equals(cid)){
            icid=Integer.parseInt(cid);
        }
        return iRouteService.findbyCid(icid,title,currentPage,pageSize,uid);
    }

    @PostMapping("/findOne")
    public ServerResponse<Release> findOne(String tid){
        System.out.println(tid);
        return iRouteService.findbyTid(Integer.parseInt(tid));
    }

    @GetMapping("/findAll")
    private ServerResponse<List<Release>> findAll(){
        return iRouteService.findAll();
    }


}
