package com.example.demo.controller;


import com.example.demo.common.Cnst;
import com.example.demo.common.ResponseCode;
import com.example.demo.common.ServerResponse;
import com.example.demo.entity.User;
import com.example.demo.service.IStatisticsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;

@RestController
@RequestMapping("/statistics")
public class StatisticsController {
    @Autowired
    IStatisticsService statisticsService;

    @PostMapping("/update")
    public ServerResponse Update(int tid, HttpSession session){
        User loginUser=(User) session.getAttribute(Cnst.CURRENT_USER);
        if(loginUser==null){
            return ServerResponse.failWithCodeMsg(ResponseCode.NEED_LOGIN.getCode(),"用户未登录！请");
        }else {
            return statisticsService.update(tid, loginUser.getUid());
        }

    }
    @PostMapping("/updateLook")
    public ServerResponse UpdateLook(int tid, HttpSession session){
        User loginUser=(User) session.getAttribute(Cnst.CURRENT_USER);
        if(loginUser==null){
            return ServerResponse.failWithCodeMsg(ResponseCode.NEED_LOGIN.getCode(),"用户未登录！请");
        }else {
            return statisticsService.updateLook(tid, loginUser.getUid());
        }

    }
}
