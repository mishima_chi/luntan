package com.example.demo.controller;

import com.example.demo.common.ServerResponse;
import com.example.demo.entity.Category;
import com.example.demo.entity.Release;
import com.example.demo.entity.User;
import com.example.demo.service.IReleaseService;
import com.example.demo.service.IRouteService;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/release")
public class ReleaseController {
    @Autowired
    private IReleaseService iReleaseService;


    @PostMapping("/tijiao")
    public ServerResponse tijiao(Release release){
        System.out.println(release);
        ServerResponse serverResponse=iReleaseService.release(release);
        return serverResponse;
    }

//    @GetMapping("/findAll")
//    public ServerResponse<List<Release>> findAll(){
//        return iReleaseService.findAll();
//    }


}
