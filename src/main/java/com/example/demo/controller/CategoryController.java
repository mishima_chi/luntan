package com.example.demo.controller;

import com.example.demo.common.ServerResponse;
import com.example.demo.entity.Category;
import com.example.demo.service.ICategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/category")
public class CategoryController {
    @Autowired
    private ICategoryService categoryService;

    @GetMapping("/findAll")
    public ServerResponse<List<Category>> findAll(){
        return categoryService.findAll();
    }
}
