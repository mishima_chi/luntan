package com.example.demo.entity;

public class Statistics {
    private int tid;
    private int uid;
    private boolean thumbs_up;
    private boolean look;

    public int getTid() {
        return tid;
    }

    public void setTid(int tid) {
        this.tid = tid;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public boolean isThumbs_up() {
        return thumbs_up;
    }

    public void setThumbs_up(boolean thumbs_up) {
        this.thumbs_up = thumbs_up;
    }

    public boolean isLook() {
        return look;
    }

    public void setLook(boolean look) {
        this.look = look;
    }


    @Override
    public String toString() {
        return "Statistics{" +
                "tid=" + tid +
                ", uid=" + uid +
                ", thumbs_up=" + thumbs_up +
                ", look=" + look +
                '}';
    }
}
