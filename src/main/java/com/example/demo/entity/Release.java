package com.example.demo.entity;

public class Release {
    private int tid;//帖子id
    private int uid;//用户id
    private String title;//标题
    private String content;//内容
    private String type;//分类
    private String release_time;//发布时间,2000-01-01 08:00:00
    private int cid;
    private int countUp;
    private int countLook;
    private String name;
    private boolean alreadyUp;


    public int getCountUp() {
        return countUp;
    }

    public void setCountUp(int countUp) {
        this.countUp = countUp;
    }

    public int getCountLook() {
        return countLook;
    }

    public void setCountLook(int countLook) {
        this.countLook = countLook;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Release(){

    }

    public boolean isAlreadyUp() {
        return alreadyUp;
    }

    public void setAlreadyUp(boolean alreadyUp) {
        this.alreadyUp = alreadyUp;
    }

    public Release(int tid, int uid, String title, String content, String type,int cid, String release_time){
        this.tid=tid;
        this.uid=uid;
        this.title=title;
        this.content=content;
        this.type=type;
        this.release_time=release_time;
        this.cid=cid;
    }
    public int getTid() {
        return tid;
    }

    public void setTid(int tid) {
        this.tid = tid;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getRelease_time() {
        return release_time;
    }

    public void setRelease_time(String release_time) {
        this.release_time = release_time;
    }

    public int getCid() {
        return cid;
    }

    public void setCid(int cid) {
        this.cid = cid;
    }

    @Override
    public String toString() {
        return "Release{" +
                "tid=" + tid +
                ", uid=" + uid +
                ", title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", type='" + type + '\'' +
                ", release_time='" + release_time + '\'' +
                ", cid=" + cid +
                ", countUp=" + countUp +
                ", countLook=" + countLook +
                ", name='" + name + '\'' +
                ", alreadyUp=" + alreadyUp +
                '}';
    }
}
