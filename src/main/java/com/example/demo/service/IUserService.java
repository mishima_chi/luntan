package com.example.demo.service;


import com.example.demo.common.ServerResponse;
import com.example.demo.entity.User;

public interface IUserService {

    ServerResponse login(String username, String password,String check);
    ServerResponse register(User user);
}
