package com.example.demo.service.impl;

import com.example.demo.common.ServerResponse;
import com.example.demo.entity.Release;
import com.example.demo.mapper.RouteMapper;
import com.example.demo.mapper.StatisticsMapper;
import com.example.demo.mapper.UserMapper;
import com.example.demo.service.IRouteService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RouteServiceImpl implements IRouteService {
    @Autowired
    private RouteMapper routeMapper;
    @Autowired
    StatisticsMapper statisticsMapper;
    @Autowired
    UserMapper userMapper;




    @Override
    public ServerResponse<PageInfo> findbyCid(int cid, String title,int currentPage, int pageSize,int uid) {
        PageHelper.startPage(currentPage,pageSize);


        List<Release> cs=routeMapper.findByCidAndRname(cid,title);
//        if(cs==null){
//            return ServerResponse.succRealse("没有找到对应的数据...");
//        }
        boolean isAlready=false;
        for(Release list : cs){
            Release release= list;
            release.setName(userMapper.idFindUser(release.getUid()));
            release.setCountLook(statisticsMapper.countLook(release.getTid()));
            release.setCountUp(statisticsMapper.countUp(release.getTid()));

            if(uid!=0 && statisticsMapper.countByUid(list.getTid(), uid)!=0){
                String is=statisticsMapper.alreadyUp(list.getTid(), uid);
                if(is.equals("0")){
                    isAlready=false;
                }else if(is.equals("1")){
                    isAlready=true;
                }
            }else {
                isAlready=false;
            }
            System.out.println(isAlready);
            release.setAlreadyUp(isAlready);
        }

        PageInfo<Release> page=new PageInfo(cs);
        return ServerResponse.succWithMsgData("succ",page);

    }

    @Override
    public ServerResponse<Release> findbyTid(int tid) {
        boolean isAlready=false;
        Release release=routeMapper.findByTid(tid);

        release.setName(userMapper.idFindUser(release.getUid()));
        release.setCountLook(statisticsMapper.countLook(release.getTid()));
        release.setCountUp(statisticsMapper.countUp(release.getTid()));
        if(statisticsMapper.alreadyUp(release.getTid(), release.getUid())==null || statisticsMapper.alreadyUp(release.getTid(), release.getUid())=="0"){
            isAlready=false;
        }else {
            isAlready=true;
        }
        release.setAlreadyUp(isAlready);
        return ServerResponse.succWithMsgData("scc",release);
    }

    @Override
    public ServerResponse<List<Release>> findAll() {
        List<Release> releases=routeMapper.findAll();
        for(Release release : releases){
            release.setName(userMapper.idFindUser(release.getUid()));
            release.setCountLook(statisticsMapper.countLook(release.getTid()));
            release.setCountUp(statisticsMapper.countUp(release.getTid()));
        }
        return ServerResponse.succWithMsgData("succ",releases);
    }

}
