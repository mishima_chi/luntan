package com.example.demo.service.impl;

import com.example.demo.common.ResponseCode;
import com.example.demo.common.ServerResponse;
import com.example.demo.entity.Release;
import com.example.demo.mapper.ReleaseMapper;
import com.example.demo.mapper.StatisticsMapper;
import com.example.demo.mapper.UserMapper;
import com.example.demo.service.IReleaseService;
import com.example.demo.service.IRouteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class ReleaseServiceImpl implements IReleaseService {
    @Autowired
    ReleaseMapper releaseMapper;


    @Override
    public ServerResponse release(Release release) {

        releaseMapper.insert(release);
        return ServerResponse.succRealse("发布成功");

    }
    }

