package com.example.demo.service.impl;

import com.example.demo.common.ServerResponse;
import com.example.demo.entity.Category;
import com.example.demo.mapper.CategoryMapper;
import com.example.demo.service.ICategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Locale;

@Service
public class CategoryServiceImpl implements ICategoryService {
    @Autowired
    CategoryMapper categoryMapper;

    @Override
    public ServerResponse<List<Category>> findAll() {
        List<Category> categoryList=categoryMapper.findAll();
        return ServerResponse.succWithMsgData("列表",categoryList);
    }
}
