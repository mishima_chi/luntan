package com.example.demo.service.impl;

import com.example.demo.common.ResponseCode;
import com.example.demo.common.ServerResponse;
import com.example.demo.mapper.StatisticsMapper;
import com.example.demo.service.IStatisticsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StatisticsServiceImpl implements IStatisticsService {
    @Autowired
    StatisticsMapper statisticsMapper;

    @Override
    public ServerResponse update(int tid, int uid) {
        //0 or 1
        int count=statisticsMapper.count(tid,uid);
        boolean isTrue=false;
        if(count==0){
            statisticsMapper.tidInsertUp(tid,uid);
            isTrue=true;
        }else{
            //查询有没有被选中
            isTrue=statisticsMapper.isTrue(tid,uid);
            if(isTrue==false){
                isTrue=true;
            }else{
                isTrue=false;
            }
            statisticsMapper.tidUpdateUp(tid,uid,isTrue);

        }
        String msg="";
        if(isTrue==false){
            msg="取消成功";
        }else{
            msg="点赞成功";
        }
        return new ServerResponse<>(ResponseCode.SUCCESS.getCode(), msg,isTrue);
    }

    @Override
    public ServerResponse updateLook(int tid, int uid) {
        int count=statisticsMapper.count(tid,uid);
        boolean isLook=false;
        if(count==0){
            statisticsMapper.tidInsertLook(tid,uid);
        }else{
            //查询有没有被选中
            isLook=statisticsMapper.isLook(tid,uid);
            if(isLook==false){
                isLook=true;
                statisticsMapper.tidUpdateLook(tid,uid,isLook);
            }


        }
        return null;

    }
}
