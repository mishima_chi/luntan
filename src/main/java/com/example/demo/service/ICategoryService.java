package com.example.demo.service;

import com.example.demo.common.ServerResponse;
import com.example.demo.entity.Category;

import java.util.List;

public interface ICategoryService {

    ServerResponse<List<Category>> findAll();
}
