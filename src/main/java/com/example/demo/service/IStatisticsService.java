package com.example.demo.service;

import com.example.demo.common.ServerResponse;

public interface IStatisticsService {
    ServerResponse update(int tid, int uid);

    ServerResponse updateLook(int tid, int uid);
}
