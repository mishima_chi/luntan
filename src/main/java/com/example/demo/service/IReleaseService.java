package com.example.demo.service;

import com.example.demo.common.ServerResponse;
import com.example.demo.entity.Release;
import com.github.pagehelper.PageInfo;

import java.util.List;

public interface IReleaseService {
    ServerResponse release(Release release);

//    ServerResponse<List<Release>> findAll();

}
