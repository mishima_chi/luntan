package com.example.demo.service;

import com.example.demo.common.ServerResponse;
import com.example.demo.entity.Release;
import com.github.pagehelper.PageInfo;

import java.util.List;

public interface IRouteService {
    ServerResponse<PageInfo> findbyCid(int cid, String ranme, int currentPage, int pageSize,int uid);

    ServerResponse<Release> findbyTid(int tid);

    ServerResponse<List<Release>> findAll();
}
