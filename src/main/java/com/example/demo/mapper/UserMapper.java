package com.example.demo.mapper;

import com.example.demo.entity.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface UserMapper {
    @Select("select * from tab_user where username=#{username} and password=#{password}")
    User login(@Param("username") String username, @Param("password") String password);

    @Select("select count(1) from tab_user where username=#{username}")
    int checkUsername(String username);

    @Select("select name from tab_user where uid=#{uid}")
    String idFindUser(int uid);

    @Insert("insert into tab_user(username,password,name,birthday,sex,telephone,email,status,code) values (#{username},#{password},#{name},#{birthday},#{sex},#{telephone},#{email},#{status},#{code})")
    int register(User user);
}
