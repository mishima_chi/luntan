package com.example.demo.mapper;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

@Mapper
public interface StatisticsMapper {


    @Select("select count(*) from statistics where thumbs_up=1 and tid=#{tid}")
    int countUp(int tid);

    @Select("select count(*) from statistics where tid=#{tid} and uid=#{uid}")
    int countByUid(int tid,int uid);

    @Select("select count(*) from statistics where look=1 and tid=#{tid}")
    int countLook(int tid);

    @Select("select thumbs_up from statistics where tid=#{tid} and uid=#{uid}")
    String alreadyUp(int tid,int uid);

    @Select("select count(*) from statistics where tid=#{tid} and uid=#{uid}")
    int count(int tid, int uid);

    @Insert("insert into statistics(tid,uid,thumbs_up) value(#{tid},#{uid},1)")
    void tidInsertUp(int tid, int uid);

    @Insert("insert into statistics(tid,uid,look) value(#{tid},#{uid},1)")
    void tidInsertLook(int tid, int uid);

    @Update("update statistics set thumbs_up=#{isTrue} where tid=#{tid} and uid=#{uid}")
    void tidUpdateUp(int tid, int uid,boolean isTrue);

    @Update("update statistics set look=#{isTrue} where tid=#{tid} and uid=#{uid}")
    void tidUpdateLook(int tid, int uid,boolean isTrue);

    @Select("select thumbs_up from statistics where tid=#{tid} and uid=#{uid}")
    boolean isTrue(int tid, int uid);
    @Select("select look from statistics where tid=#{tid} and uid=#{uid}")
    boolean isLook(int tid, int uid);
}
