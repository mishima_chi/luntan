package com.example.demo.mapper;

import com.example.demo.entity.Category;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface CategoryMapper  {
    //列表
    @Select("select * from category order by cid asc")
    List<Category> findAll();

    @Select("select cid from category where cname#{cname}")
    int findCid(int cname);
}
