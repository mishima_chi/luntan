package com.example.demo.mapper;

import com.example.demo.common.ServerResponse;
import com.example.demo.entity.Release;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface ReleaseMapper {
    @Insert("insert into release1(uid,title,content,type,cid,release_time) values (#{uid},#{title},#{content},#{type},#{cid},#{release_time})")
    int insert(Release release);

    @Select("select * from release1")
    List<Release> findAll();
//        private int tid;//帖子id
//        private int uid;//用户id  user表查询
//        private String title;//标题
//        private String content;//内容
//        private String type;//分类
//        private String release_time;//发布时间,2000-01-01 08:00:00
//        private String countUp;   statistics查询
//        private String countLook; statistics查询
}
