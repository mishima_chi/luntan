package com.example.demo.mapper;

import com.example.demo.entity.Release;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface RouteMapper {
    @Select({"<script>"+
            "SELECT * FROM release1 " +
            "WHERE 1 = 1 " +
            "<if test='cid!=0'> and cid=#{cid}</if> " +
            "<if test='title!=\"\" and title!=\"null\"'> and title like '%${title}%'</if> " +
            "</script>"
    })
    List<Release> findByCidAndRname(int cid, String title);

    @Select("select * from release1 where tid=${tid}")
    Release findByTid(@Param("tid") int tid);

    @Select("select * from release1")
    List<Release> findAll();
}
